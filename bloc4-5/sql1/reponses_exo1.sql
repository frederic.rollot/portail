-- Quels sont les différents libellés des catégories socio-professionnelles par ordre alphabétique croissant ?

SELECT DISTINCT categorie FROM evolution ORDER BY categorie;

-- Combien de catégories différentes sont utilisées dans ce jeu de données ?

SELECT count(DISTINCT categorie) FROM evolution;

-- Quel est le code postal de Caullery ?

SELECT code from ville where nom="Caullery";

-- Affichez toutes les informations de table évolution pour la ville de Caullery (en utilisant le code postal), triées par effectifs croissants.

SELECT * FROM evolution WHERE code="59140" order by categorie;

-- Quels sont les codes postaux des villes ayant des catégories sociaux-professionnelles qui comptent plus de 2000 individus ?

SELECT distinct code from evolution where effectif > 2000;

-- Combien y a t'il de femmes agricultrices dans le Nord ?

SELECT categorie, genre, sum(effectif)
	FROM evolution
	WHERE categorie="Agriculteurs Exploitants" AND genre="Femmes";

-- Quel est le nombre moyen d'employés dans les villes du Nord ?

SELECT AVG(effectif) AS Nb_moy_employes
	FROM evolution
	WHERE categorie="Employés";

--Afficher les villes dont les noms commencent par la lettre K
-- Non demandé dans le TP, pas au programme

SELECT nom from ville where nom like "K%";
