import sqlite3
import matplotlib.pyplot as plt
import timeit

#conn=sqlite3.connect("../sql1/categories-socio-nord.db")
conn=sqlite3.connect("/home/pupin/Syncro/Enseignement/2019-2020/DIU-EIL/SQL/categories-socio-nord-tp2.db")

cur=conn.cursor()

cur.execute("SELECT ville.code, categorie, genre, effectif \
    FROM ville JOIN evolution ON ville.code=evolution.code \
    WHERE nom='Caullery'")

donnees=cur.fetchall()
print(donnees)
print()

# Ou de façon équivalente
cur.execute("""SELECT v.code, categorie, genre, effectif
    FROM ville AS v JOIN evolution AS e ON v.code=e.code
    WHERE nom='Caullery'""")
donnee=cur.fetchone()
while(donnee!= None):
    print(donnee)
    donnee=cur.fetchone()

# Récupération du nombre total de femmes
cur.execute("SELECT SUM(effectif) FROM evolution WHERE genre='Femmes'")
femmes=cur.fetchone()

print(femmes)

# Calcul du pourcentage de femmes
cur.execute("SELECT 100*"+str(femmes[0])+"/SUM(effectif) FROM evolution")

pourcentage=cur.fetchone()
print("Le pourcentage de femmes est", pourcentage[0],"%")

# Affichage données question 3

cur.execute("SELECT codeINSEE, codePostal, commune, categorie, genre, effectif \
            FROM evolution JOIN correspondance ON codeINSEE=code \
            WHERE commune=?", ('VILLENEUVE-D\'ASCQ',))

res = cur.fetchall()
for ligne in res:
    print(ligne)


# Affichage données question 4

def select_where(code):
    cur.execute("SELECT codeINSEE, codePostal, commune, categorie, genre, effectif \
            FROM evolution JOIN correspondance ON codeINSEE=code \
            WHERE codePostal=?", (code,))

    return cur.fetchall()

def select_for_if(code):
    cur.execute("SELECT codeINSEE, codePostal, commune, categorie, genre, effectif \
            FROM evolution JOIN correspondance ON codeINSEE=code")

    donnees = cur.fetchall()
    
    res = []
    for ligne in donnees:
        if ligne[1] == code:
            res.append(ligne)
            
    return res

print("Est-ce que les 2 fonctions renvoient à la même valeur ?",
      select_where(59153) == select_for_if(59153))
tps_where = timeit.timeit(lambda: select_where(59153), number = 1)
print("Temps en utilisant WHERE :", tps_where)

tps_for_if = timeit.timeit(lambda: select_for_if(59153), number = 1)
print("Temps en utilisant for et if :", tps_for_if)

# Récupération des données pour le camembert selon un code postal et un genre passés en paramètres

def camembert(code, genre):
    cur.execute("SELECT SUM(effectif), categorie \
        FROM evolution JOIN correspondance ON code=codeINSEE \
        WHERE codePostal=? AND genre=? GROUP BY categorie", (code, genre))

    donnees=cur.fetchall()

    libelles=[]
    pourcentages=[]

    for result in donnees:
        pourcentages.append(result[0])
        libelles.append(result[1])
    
    cur.execute("SELECT commune \
    FROM evolution JOIN correspondance ON code=codeINSEE \
    WHERE codePostal=?", (code,))

    nom_ville=cur.fetchone()[0]

    plt.title("Effectifs pour les "+ genre + " de la ville de "+nom_ville)
    plt.pie(pourcentages, labels=libelles)

    plt.show()

camembert('59152','Femmes')

# Récupération des données pour le camembert selon un code postal passé en paramètre

def camembert(code):
    cur.execute("SELECT  SUM(effectif), categorie \
    FROM evolution JOIN correspondance ON code=codeINSEE \
    WHERE codePostal=? GROUP BY categorie", (code,))

    donnees=cur.fetchall()

    libelles=[]
    pourcentages=[]

    for result in donnees:
        pourcentages.append(result[0])
        libelles.append(result[1])

    cur.execute("SELECT commune \
    FROM evolution JOIN correspondance ON code=codeINSEE \
    WHERE codePostal=?", (code,))

    nom_ville=cur.fetchone()[0]

    plt.title("Effectifs globaux pour la ville de "+nom_ville)
    plt.pie(pourcentages, labels=libelles)

    plt.show()
    
camembert('59152')

conn.close()