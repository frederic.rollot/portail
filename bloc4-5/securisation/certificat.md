% Les certificats électroniques
% DIU-EIL

# Généralités sur la cryptographie


## *Cryptographie*

-   La cryptographie est la science du **chiffrement**,
-   opération qui permet à deux individus d'échanger des messages 
    de manière confidentielle à travers un canal **non sécurisé**.
    
    ![](./figs/com_A_B.png)
-   actuellement : problèmes d'intégrité de données, d'authentification
    d'entité et de signature de documents.


## *clair* et *chiffré*

-   Pour réaliser cet objectif, les deux individus conviennent d'un
    **secret**, permettant de transformer le message appelé *clair*, en
    un message **inintelligible** appelé *chiffré* afin que l'attaquant
    éventuel ne puisse en prendre connaissance.
-   Cette transformation doit
    être **réversible** : si on connait le secret et le *chiffré*, alors on
    peut reconstituer le *clair*.


## *Clés*

-   Un bon système de chiffrement ne doit et ne peut pas reposer
    uniquement sur le caractère secret de la méthode employée 
    ([Principe de Kerckhoffs](https://fr.wikipedia.org/wiki/Principe_de_Kerckhoffs)).
    
    <div class="notes">
    Si tel était le cas, il faudrait changer de
    méthode dès qu'elle serait compromise, ce qui peut être coûteux,
    voir impossible. Au contraire, il faut considérer que l'attaquant
    connait le secret.
    principe de Kerchkoff
    
    </div>
-   La sécurité d'un système de chiffrement doit reposer sur :
    
    1.  La difficulté à calculer le clair à partir du chiffré sans
        la connaissance du secret ,
    2.  La difficulté à retrouver le secret ;
    
    <div class="notes">
    attaque à clairs connus, à clair choisis
    
    </div>
-   Secret = mot(s), ou  nombre(s) appelés *clés*.
    
    <div class="notes">
    La méthode de chiffrement explique :
    
    -   comment à partir des clés et du clair, on produit le chiffré;
    -   comment, à partir du chiffré et des clés, on produit le clair.
    
    </div>


# Chiffrement symétrique


## Définition

-   Dans les méthodes de chiffrement symétrique, la clé est **unique** :
    elle sert à la fois au **chiffrement et au déchiffrement**.
    
    <div class="notes">
    Si Alice (A) veut envoyer un message chiffré à Bob (B), elle
    chiffre le message en utilisant une clé partagée entre eux deux
    Pour déchiffrer le message, Bob utilise la clé.
    
    </div>
    
    ![](./figs/sym_A_B.png)


## Inconvénients

1.  La clé doit d'abord être échangée entre les protagonistes, au
    moyen d'un **canal sûr**.
    ![](./figs/sym_echange_cle.png)
2.  Si Alice veut échanger de manière sécurisée avec $n-1$
    participants, alors $n-1$ clés sont nécessaires.
3.  Par conséquent, si $n$ personnes veulent communiquer de manière
    sécurisée, alors $\frac{n(n-1)}{2}$ clés sont nécessaires.


## Exemples de chiffrements symétriques

-   Chiffrements par bloc :
    -   blowfish
    -   aes
    -   serpent
-   Chiffrements par flot :
    -   RC4


# Chiffrement asymétrique


## Principe

Dans les méthodes de chiffrement asymétrique, chaque participant
possède une **paire de clés** :

-   une **clé publique**, que (comme son nom l'indique) tout le
    monde peut obtenir;
-   une **clé privée**,  propriété exclusive de son propriétaire.

![](./figs/Asym_A.png)


## Propriétés

Ces deux clés sont **liées** l'une à l'autre.

Le système repose sur la propriété suivante :

-   un message chiffré avec la clé publique ne pourra être déchiffré
    qu'avec la clé privée correspondante

![](./figs/Asym_CD_2.png)


## Scénario

-   Lorsque Bob veut envoyer un message de manière confidentielle à
    Alice, il utilise la **clé publique d'Alice** : Seule Alice pourra
    déchiffrer le message.

![](./figs/Asym_B_A.png)


## Quelques systèmes asymétriques

-   Systèmes :
    -   RSA : Ronald Shamir Adleman, 1977
    -   Mc Eliece, 1978
    -   Goldwasser-Micali , 1982
    -   El Gamal : El Gamal , 1984
-   Inconvénients :
    -   Complexité algorithmique

<div class="notes">
-   Les système asymétriques sont utilisés pour communiquer une clé
    d'un chiffrement symétrique.
-   Cette clé est générée aléatoirement et est appelée clé de session.
-   Reposent sur des problèmes mathématiques :
    -   factorisation
    -   réseaux euclidiens
    -   lograithme discret
    -   codes correcteurs
-   firefox : about:config, security.ssl

</div>


## Vérifier

Lorsque Bob veut vérifier que le message reçu provient bien d'Alice.

-   il calcule un condensé du message $h(m)$ ;
-   il déchiffre la signature à l'aide de la clé publique de d'Alice :
    $D_\mbox{Kpub}(s)$;
-   Si $h(m) = D_\mbox{ Kpub}(s)$, alors le message provient bien d'Alice.

![](./figs/Asym_A_B_sign.png)


# Signature


## Scénario

-   Bob correspond avec Alice et souhaite être sûr :
    -   Que les messages qu'il reçoit ont bien été écrits par Alice et/ou
    -   Qu'Alice a bien consulté un message.


## Problème &#x2026;

-   On souhaite pouvoir garantir :
    
    -   l'authenfication un document : provient-il bien de son auteur :
    -   l'engagement de ce dernier à avoir consulté celui-ci
    
    Il s'agit du problème de la signature, le dernier point implique 
    que toute modification ultérieure invaliderait la signature.

<div class="notes">
la signature électronique doit être fonction du document et de son
auteur

</div>


## Mise en oeuvre (1)

-   La mise en oeuvre de cette signature profite du caractère
    réversible de certains systèmes de chiffrement asymétrique, comme
    RSA.
-   $m \to E_\mbox{ Kpub}(m)=c \to D_\mbox{ priv}(c) = m$

$m \to D_\mbox{ Kpriv}(m)=s \to E_\mbox{ Kpub}(s) = m$
![](./figs/Asym_CD_1.png)

<div class="notes">
la signature est bien attachée au document et à la personne

</div>


## Mise en oeuvre (2)

-   Deux défauts majeurs :
    -   produire et vérifier la signature est long et coûteux ;
    -   la signature est aussi grosse que le document.
-   Sans rentrer dans les détails, une fonction de **hachage**
    (parfaite) $h$ permet d'obtenir un condensé d'un message. Il
    s'agit d'une fonction :
    
    -   difficilement réversible ;
    -   injective : Si $h(m)=h(m')$, alors $m=m'$.
    
    <div class="notes">
    -   de telles fonctions n'existent pas
    -   les collisions sont difficiles à trouver
    
    </div>


## Signer

Lorsque Alice veut signer son message,

-   elle calcule un condensé du message $h(m)$;
-   elle chiffre le condensé avec sa clé privée $s = E_\mbox{ Kpriv}(h(m))$;
-   elle envoie le message $m$, accompagné de la signature $s$.

![](./figs/Asym_A_B_sign.png)


# Certificat électronique


## Problème

-   Lorsqu'une entité veut communiquer avec un large public, il lui
    suffit de diffuser sa clé publique.
-   La diffusion de la clé publique pose néanmoins problème,
    puisqu'elle se fait au travers d'un canal non sécurisé.
-   Que ce passe-t-il lorsque un attaquant se positionne **entre l'entité
    et son public** ?


## Attaque de l'homme du milieu

-   Alice souhaite communiquer à Bob sa clé publique.
-   L'attaquant Charlie parvient, par un moyen ou un autre, à
    intercepter la diffusion de la clé publique ;
-   Il envoie à Bob sa propre clé publique.

![](./figs/Asym_MIM_steal.png)


## Attaque de l'homme du milieu

-   L'usurpation fonctionne alors dans les deux sens :
-   Si Alice signe un message, Charlie le signe avec sa clé privée;
-   Si Bob envoie un message chiffré à Alice, il utilise la
    clé de Charlie.

![](./figs/Asym_MIM.png)


## Les certificats

Les **certificats électroniques** sont une réponse à ce problème de
diffusion non sécurisée de la clé publique.

<div class="notes">
deux modèles 

-   modèle hiérarchique : basé sur des autorités de certification
-   modèle réseau : pgp, mail

</div>


## autorité de certification

-   Un certificat met en jeu une autre entité : l'**autorité de
    certification**.
-   Il s'agit d'organismes dûments enregistrés et certifiés auprès des
    autorités publiques.
-   Les systèmes d'exploitation et navigateurs web incluent nativement
    les listes des clés publiques de ces autorités de certification.


## définition

Un certificat électronique est un ensemble de
données contenant :

-   des informations d'indentification (nom, email, &#x2026;)
-   une clé publique
-   une signature des données précédentes obtenue avec la clé privée
    d'une autorité de certification.


## création du certificat (1)

Lorsque Alice veut diffuser sa clé publique, 

-   elle effectue une demande auprès d'une autorité de
    certification. Celle ci reçoit la clé publique et l'identité
    d'Alice.
-   Après avoir vérifié la clé publique et l'identité d'Alice par des
    moyens conventionnels, elle crée un certificat contenant les
    données et la signature calcuélée avec sa clé privée.

## création du certificat (2)

![](./figs/Certification.png)

    
## utilisation du certificat (1)

Bob souhaite communiquer avec Alice. Celle-ci lui envoie son certificat.

Bob peut alors vérifier l'intégrité du certificat en utilisant la
clé publique de l'autorité de certification.

## utilisation du certificat (2)
S'il authentifie le certificat, alors il peut utiliser la clé publique 
qu'il contient pour échanger avec Alice.

![](./figs/Asym_certif_B_A.png)

