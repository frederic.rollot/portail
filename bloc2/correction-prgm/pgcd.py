## Logique de Hoare pour le pgcd : cet exemple montre un exemple de propagation
## de pré- et post-conditions sur un programme calculant le pgcd.

## Pour montrer qu'à la fin du calcul a contient la valeur de pgcd(x,y)
## Nous usilisons le lemme suivant :
##
## Pour tout x, y si d divise x et y alors d divise x% y.
##
## Démonstration
##
## Si d divise x et y on a x == d * x' et y == d * y'. Or x == h * y+ x % y si bien que
## d * x' == h * d * y' + x % y et x % y  = d * (x'- h * y'). Ainsi d divise bien x % y.
##

## pgcd(834, 766) == pgcd(834,766)
a=834
## pgcd(766,a % 766) == pgcd(834,766)
b=766
## pgcd(a,b) == pgcd(834,766)
while b > 0:
    ## pgcd(a, b) == pgcd(834,766) and  b > 0
    ## implique
    ## pgcd(b,a % b) == pgcd(834,766)
    c=b
    ## pgcd(c,a % b) == pgcd(834,766) 
    b=a % b
    ## pgcd(c,b) == pgcd(834,766) 
    a=c
    ## pgcd(a,b) == pgcd(834,766)
## pgcd(a,b) == pgcd(834,766)  and b==0
## implique
## pgcd(a,b) == pgcd(834,766)
