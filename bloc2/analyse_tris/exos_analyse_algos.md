---
title: Exercices analyse de complexité d'algorithmes
author: Équipe pédagogique DIU EIL Lille
date: juin 2019
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---
**Objectif** réaliser une analyse de complexité théorique d'algorithmes simples.

# Calcul de puissances

Une fonction de calcul de la puissance $n$-ième d'un nombre flottant est réalisée ci-dessous en utilisant deux algos différents. Les deux réalisations respectent cette documentation :

```python
    '''
    renvoie la puissance n-ième de x
    
    :param x: (int ou float) un nombre
    :param n: (int) un exposant
    :return: (float) puissance n-ieme de x
    :CU: n >= 0
    :Exemples:
    
    >>> puissance(2, 0)
    1.0
    >>> puissance(2, 10)
    1024.0
    '''
```

La question est d'analyser le nombre de multiplications de nombres flottants et de (tenter de) l'exprimer en fonction de l'exposant $n$. 

## Première méthode

Algorithme suivant la définition de la puissance $n$-ième d'un nombre comme étant ce nombre multiplié par lui-même $n$ fois.


```python
def puissance1(x, n):
    res = 1.
    for _ in range(n):
        res *= x
    return res
```




## Seconde méthode : exponentiation rapide

Cette seconde méthode s'appuie sur la remarque suivante :

$$ x^n = \left\{
\begin{array}{ll}
(x^{2})^p & \mbox{si }n=2p\\
(x^2)^p\times x & \mbox{si }n=2p+1
\end{array}
\right.$$


```python
def puissance2(x, n):
    r, s, k = 1., float(x), n
    while k != 0:
        if k % 2 == 1:
            r *= s
        s *= s
        k //= 2
    return r
```



# Recherche dichotomique dans une liste triée

Une fonction de recherche d'un élément $x$ dans une liste triée $\ell$ est réalisée en suivant l'algorithme de recherche dichotomique. On en donne deux versions : une itérative, et une récursive, les deux versions respectant cette documentation :

```python
    '''
    renvoie True si x est dans la tranche des éléments de l d'indice compris entre d et f inclus
    et False sinon
    
    :param x: (any) un élément quelconque
    :param l: (list) 
    :param d, f: (int) indices de début et de fin de tranche de l 
         dans laquelle la recherche doit s'effectuer
         valeur par défaut pour d : 0 (premier indice)
         valeur par défaut pour f : len(l)-1 (dernier indice)
    :return: (bool)
       - True si x == l[i] pour un certain indice i compris entre d et f inclus
       - False sinon
    :CU: x doit être comparable à chaque élément de l 
         et la liste l doit être triée dans l'ordre croissant
         et 0 <= d <= f < len(l) 
         Remarque : cette dernière contrainte interdit la recherche dans une liste vide
         
    >>> recherche_dicho(1, [0, 2, 4, 6, 8], 0, 4) # recherche dans toute la liste
    False
    >>> recherche_dicho(6, [0, 2, 4, 6, 8], 0, 4) # recherche dans toute la liste
    True
    >>> recherche_dicho(6, [0, 2, 4, 6, 8], 0, 2) # recherche pour des indices compris entre 0 et 2
    False
    '''
```

La question est d'analyser le nombre de comparaisons et de (tenter de) l'exprimer en fonction de la longueur $n$ de la liste triée. 

## Première version


```python
def recherche_dicho1(x, l, d = 0, f = None):
    if f is None: f = len(l)-1
    while d < f:
        m = (d + f) // 2
        if x <= l[m]:
            f = m 
        else:
            d = m + 1
    return x == l[d]
```




## Version récursive

<!--
```python
def recherche_dicho2(x, l, d = 0, f = None):
    if f is None: f = len(l)-1
    if d == f:
        return x == l[d]
    else:
        m = (d + f) // 2
        if x <= l[m]:
            return recherche_dicho2(x, l, d, m)
        else:
            return recherche_dicho2(x, l, m + 1, f)
```
-->

```python
def recherche_dicho2(x, l, d = 0, f = None):
    def recherche_dicho2_aux(x, l , d, f) :
        if d == f:
            return x == l[d]
        else:
            m = (d + f) // 2
            if x <= l[m]:
                return recherche_dicho2_aux(x, l, d, m)
            else:
                return recherche_dicho2_aux(x, l, m + 1, f)
            
    if f is None: f = len(l)-1            
    return recherche_dicho2_aux(x, l, d , f);
```

