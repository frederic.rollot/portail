Bloc 2 - Algorithmique
======================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Premières activités sur les tris
================================

vendredi 8 janvier 2021

Travaux dirigés - Activité d'informatique sans ordinateur

* [Tris sans ordi](tri-sans-ordi/Readme.md)

mardi 12 janvier 2021

Travaux pratiques 

* [Reconnaître et programmer les tris](tris_anonymes/Readme.md)

Ressources sur les tris
-----------------------

* Présentation et animation des tris sur le site
  [lwh.free.fr/](http://lwh.free.fr/pages/algo/tri/tri.htm)
* Des algorithmes de tri visualisés avec des danses folkloriques sur la
  [chaîne vidéo AlgoRythmics](https://www.youtube.com/user/AlgoRythmics/videos)


Analyse des algorithmes (de tris)
=================================

mardi 12 janvier 2021  
mercredi 13 janvier 2021

Travaux pratiques 
* [Analyse en temps d'exécution des tris](analyse_tris/Readme.md)

Présentation
* _Analyse théorique des algorithmes, illustration sur les algorithmes
  de tri_
  [support de présentation PDF](analyse_theorique_algos_tris.pdf)
  / [fichier source Markdown](analyse_tris/analyse_theorique_algos_tris.md)


<!-- eof -->
