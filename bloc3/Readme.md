Bloc 3 - Architectures matérielles et robotique, systèmes et réseaux
====================================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

Prise en main de l'environnement
================================

jeudi 7 et vendredi 8 janvier 2021

* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Très et trop brève introduction
  - Découverte de l'interpréteur de commandes
* [GitLab](seance0/gitlab.md)
  - Découverte de l'environnement GitLab
* [Initiation à UNIX, à l'interpréteur de commandes](seance0/shell.md)
  - Utiliser l'interpréteur de commandes
  - ...

Architecture des machines informatiques et des systèmes d'exploitation
======================================================================

vendredi 8 janvier 2021

conférence de Gilles Grimaud, « _Turing, von Neumann, ... architecture
des machines informatiques et des systèmes d'exploitation_ ».

* Machine de Turing
  [page Wikipedia fr](https://fr.wikipedia.org/wiki/Machine_de_Turing)
* article fondateur d'Alan Turing, 1936 _« On Computable Numbers, with
  an Application to the Entscheidungsproblem »_
 - notes 10 et 11 de la page Wikipedia [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing#cite_note-on_computable_numbers-10) 
* _Le modèle d’architecture de von Neumann_ par Sacha Krakowiak, sur
  [interstices.info/](https://interstices.info/le-modele-darchitecture-de-von-neumann/)

Système d'exploitation
======================

vendredi 8 janvier 2021

présentation par Philippe Marquet « _Système d'exploitation — système
de fichiers, processus, shell_ ». 
	
* support de présentation
	[4 pages par page](psfssh/psfssh-4up.pdf) /
	[1 page par page](psfssh/psfssh-slide.pdf) /
	[source Markdown](psfssh/psfssh.md)

<!-- eof -->
