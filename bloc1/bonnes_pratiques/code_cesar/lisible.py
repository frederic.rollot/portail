# + des identificateurs lisibles et plus significatifs : un code plus compréhensible
# - pas réutilisable : il faut recharger le script pour changer le phrase ou le décalage
# - difficile à comprendre, donc à corriger ou modifier
# - mélange du calcul et de l'affichage : pas de résultat exploitable
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code


phrase = "Aujourd'hui, est le troisieme jour du DIU."
decalage = 13
liste_mots = phrase.split()
liste_code = []
for mot in liste_mots :
    if len(mot) < 3:
        code_mot = mot
    else:
        code_mot = ''
        for lettre in mot :
            if ord(lettre) >= 97 and ord(lettre) <= 122:
                code_lettre = chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
            else:
                code_lettre = lettre
            code_mot = code_mot + code_lettre
    
    liste_code.append(code_mot)    
print( phrase + ' -> ' +  ' '.join(liste_code) )
