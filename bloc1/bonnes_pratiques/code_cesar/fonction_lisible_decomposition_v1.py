# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + quelques commentaires aident à la compréhension
# - mais le code reste malgré tout difficile à comprendre, donc à corriger ou modifier
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code


def code_phrase(phrase, decalage = 1):
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        # code un mot
        if len(mot) < 3:
            code_mot = mot
        else:
            # code chaque lettre du mot
            code_mot = ''
            for lettre in mot :
                # code une lettre du mot
                if ord(lettre) >= 97 and ord(lettre) <= 122:
                    code_lettre = chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
                else:
                    code_lettre = lettre
                code_mot = code_mot + code_lettre
        
        liste_code.append(code_mot)
    return ' '.join(liste_code)

def affiche_code_phrase(phrase, decalage = 1):    
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )

        
phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)    



#
#
# la séparation du calcul et de l'affichage permet de composer la fonction : on peut la réutiliser pour définir d'autres traitements
def code_phrase_ROT13(phrase):
    return code_phrase(phrase,13)

#code_phrase_ROT13(code_phrase_ROT13(phrase))

def code_fichier(fichier_texte):
    destination = open("fichier_code.txt","w",encoding="utf8")
    with open(fichier_texte,"r",encoding="utf8") as source:
        for ligne_texte in source:
            destination.write(code_phrase_ROT13(ligne_texte)+'\n')
            
# code_fichier("code.txt")
            
#
#
# et si on veut des entrées :
def interface_code():
    phrase = input("phrase à codée ? ")
    decalage = int(input("quel décalage ? "))
    affiche_code_phrase(phrase, decalage)

#interface_code()

