# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + de petites fonctions dont le code est facile à lire et donc à faire évoluer
# + la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
# - des constantes numériques explicites
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code


def est_lettre(car):
    return ord(car) >= 97 and ord(car) <= 122

def code_lettre(lettre, decalage):
    return chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )

def code_cesar_caractere(car, decalage = 1):
    if est_lettre(car):
        return code_lettre(car, decalage)
    else:
        return car

def code_cesar_mot(mot, decalage = 1):
    result = ''
    for lettre in mot :
        result = result + code_cesar_caractere(lettre, decalage)
    return result

def code_mot(mot, decalage = 1):
    if len(mot) < 3:
        return mot
    else:
        return code_cesar_mot(mot, decalage)

def code_phrase(phrase, decalage = 1):
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        liste_code.append(code_mot(mot,decalage))    
    return ' '.join(liste_code)


def affiche_code_phrase(phrase, decalage = 1):    
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )
    

phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)    



#
#
# si on veut également coder les lettres majuscules, on identifie facilement quel point du programme doit être modifié
# de plus la modification a une portée limitée : on ne touche pas à ce qui n'a pas à l'être, on limite les risques de "tout casser"
'''
def est_lettre(car):
    return est_lettre_minuscule(car) or est_lettre_majuscule(car)
def est_lettre_minuscule(car):
    return ord(car) >= 97 and ord(car) < 122
def est_lettre_majuscule(car):
    return car >= 'A' and car < 'Z'

def code_lettre(lettre, decalage):
    if est_lettre_minuscule(lettre):
        return chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
    else :
        return chr ( 65 + (ord(lettre) - 65 + decalage) % 26 )

affiche_code_phrase(phrase, 13)
'''