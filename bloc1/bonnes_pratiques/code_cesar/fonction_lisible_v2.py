# + des identificateurs lisibles
# + une fonction paramétrée, plus facilement réutilisable
# + quelques commentaires aident à la compréhension
# - mais le code reste malgré tout difficile à comprendre, donc à corriger ou modifier
# - mélange du calcul et de l'affichage : pas de résultat exploitable, limite la réutilisable
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code


def affiche_code_phrase(phrase, decalage = 1):
    # code la phrase
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        # code un mot
        if len(mot) < 3:
            code_mot = mot
        else:
            # code chaque lettre du mot
            code_mot = ''
            for lettre in mot :
                # code une lettre du mot
                if ord(lettre) >= 97 and ord(lettre) <= 122:
                    code_lettre = chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
                else:
                    code_lettre = lettre
                code_mot = code_mot + code_lettre
        
        liste_code.append(code_mot)
    # affichage de la phrase codée
    print( phrase + ' -> ' +  ' '.join(liste_code) )
    
phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)


#
#
# grâce à la fonction paramétrée on peut réutiliser le code
"""
liste_phrases = ["une phrase", "la seconde phrase", "et une troisieme"]
for phrase in liste_phrases:
    affiche_code_phrase(phrase)
"""