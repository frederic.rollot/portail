---
title: Bonnes pratiques en programmation
date: janvier 2021
author: Équipe pédagogique DIU EIL Lille
---


## Objectifs

Aborder des bonnes pratiques d'écriture de code.

Pour un code, <q>Ca marche</q> ne suffit pas.


 * *Un code est lu beaucoup plus souvent qu'il n'est écrit*. Un code difficile à lire ne sera pas réutilisé.
 * Il est donc essentiel pour le programmeur d'investir dans la manière d'écrire son code.
 * Le temps consacré à bien écrire le code est du temps gagné pour la suite.


## Exemple

 * [Je fais quoi ?](code_cesar/illisible.py) 
 * [Et moi ?](code_cesar/fonction_lisible_decomposition_constantes.py)


Un programme

1. doit être lisible et clair
2. doit séparer calculs et interface homme/machine
2. doit être modulaire : découpé en petits composants faciles à comprendre, valider et modifier
2. doit être documenté : spécification
2. doit être testé : validation



# Fil conducteur 

Le chiffrement par décalage (ou [*code César*](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage)) consiste  à remplacer dans une chaines de caractères une lettre par une autre, obtenue à une distance fixe, appelée *décalage*, dans l'ordre de l'alphabet, considérant l'alphabet <q>cyclique</q> : la lettre `a`suit la lettre `z`.

**But**  
Ecrire un programme qui produit un chiffrement par décalage d'une phrase. Dans cette phrase, seuls les mots de plus de 2 caractères seront codés et seules les lettres minuscules seront concernées.


# Première version : tout ce qu'il ne faut pas faire

[Small is not always beautiful.](code_cesar/illisible.py) 


Beaucoup de défauts sont concentrés dans ces quelques lignes :

> **+**
>  * **rien !**
>
> **-**
>  *  pas lisible 
>  *  par réutilisable : il faut recharger le script pour changer le phrase ou le décalage
>  *  difficile à comprendre, donc à corriger ou modifier
>  *  mélange du calcul et de l'affichage : pas de résultat exploitable
>  *  pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  *  pas de test de validation du code



NB : heureusement Python impose dans sa syntaxe l'indentation, sinon cela aurait pu être pire.


# Deuxième version : des identificateurs explicites

[Il faut utiliser des **identificateurs explicites**.](code_cesar/lisible.py) 

> **+**
>  * **des identificateurs lisibles et significatifs : un code plus compréhensible**
> 
> **-**
>  *  pas réutilisable : il faut recharger le script pour changer le phrase ou le décalage
>  *  difficile à comprendre, donc à corriger ou modifier
>  *  mélange du calcul et de l'affichage : pas de résultat exploitable
>  *  pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  *  pas de test de validation du code

# Troisième version : des fonctions paramétrées

Pour permettre la réutilisation il faut définir des fonctions.

Attention : [il ne faut **surtout pas** utiliser input pour intégrer les données](code_cesar/fonction_lisible_v1_horreur.py) (donc : exemple à oublier !)

Les entrées "intégrées à la fonction" rendent la fonction impossible à réutiliser dans un autre contexte.

[Il faut définir des fonctions paramétrées.](code_cesar/fonction_lisible_v2.py) 

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * **une fonction paramétrée, plus facilement réutilisable**
>  * **quelques commentaires aident à la compréhension**
> 
> **-**
>  * **mais le code reste malgré tout difficile à comprendre, donc à corriger ou modifier**
>  * mélange du calcul et de l'affichage : pas de résultat exploitable, limite la réutilisable
>  * pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  * pas de test de validation du code

Grâce à la fonction paramétrée on peut réutiliser le code. Par exemple :
```python
liste_phrases = ["une phrase", "la seconde phrase", "et une troisieme"]
for phrase in liste_phrases:
    affiche_code_phrase(phrase)
```

# Quatrième version : diviser pour régner

Pour maitriser le code, il faut le décomposer.

[Il faut **séparer** calculs et affichage.](code_cesar/fonction_lisible_decomposition_v1.py) 

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * une fonction paramétrée, plus facilement réutilisable
>  * quelques commentaires aident à la compréhension
>  * **séparation du calcul et de l'affichage : le calcul peut être réutilisé**
> 
> **-**
>  * mais le code reste malgré tout difficile à comprendre, donc à corriger ou modifier
>  * pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  * pas de test de validation du code

La séparation du calcul et de l'affichage permet de composer la fonction : on peut la réutiliser pour définir d'autres traitements. Par exemple :
```python
def code_phrase_ROT13(phrase):
    return code_phrase(phrase,13)

def code_fichier(fichier_texte):
    destination = open("fichier_code.txt","w",encoding="utf8")
    with open(fichier_texte,"r",encoding="utf8") as source:
        for ligne_texte in source:
            destination.write(code_phrase_ROT13(ligne_texte)+'\n')
```


Mais la décomposition doit aller beaucoup plus loin.

[Il faut **décomposer** le code en de nombreuses <q>petites</q> fonctions.](code_cesar/fonction_lisible_decomposition_v2.py) 

Chaque fonction doit être en charge d'une étape du traitement. 
Principe de **responsabilité unique** ou de **séparation des préoccupations**.

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * une fonction paramétrée, plus facilement réutilisable
>  * séparation du calcul et de l'affichage : le calcul peut être réutilisé
>  * **de petites fonctions dont le code est facile à lire et donc à faire évoluer**
>  * **la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus**
> 
> **-**
>  * des constantes numériques explicites
>  * pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  * pas de test de validation du code

[Autre décomposition, avec des <q>sous-fonctions</q>](code_cesar/fonction_lisible_decomposition_v2_sous-fonctions.py) 


On maitrise mieux le code, on peut donc plus facilement le faire évoluer. Par exemple, si on veut maintenant également coder les lettres majuscules, on identifie facilement quel point du programme doit être modifié. De plus la modification a une portée limitée : on ne touche pas à ce qui n'a pas à l'être, on limite les risques de "tout casser"
```python
def est_lettre(car):
    return est_lettre_minuscule(car) or est_lettre_majuscule(car)
def est_lettre_minuscule(car):
    return ord(car) >= 97 and ord(car) < 122
def est_lettre_majuscule(car):
    return car >= 'A' and car < 'Z'

def code_lettre(lettre, decalage):
    if est_lettre_minuscule(lettre):
        return chr ( 97 + (ord(lettre) - 97 + decalage) % 26 )
    else :
        return chr ( 65 + (ord(lettre) - 65 + decalage) % 26 )
```
Le reste du code est inchangé.





# Cinquième version : nommer les constantes 

Toujours pour rendre le code plus explicite et plus facile à modifier.

[Il faut **nommer** les constantes littérales.](code_cesar/fonction_lisible_decomposition_constantes.py) 

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * une fonction paramétrée, plus facilement réutilisable
>  * séparation du calcul et de l'affichage : le calcul peut être réutilisé
>  * de petites fonctions dont le code est facile à lire et donc à faire évoluer
>  * la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
>  * **des constantes nommées, donc plus explicites et plus faciles à changer**
>  
> **-**
>  * pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
>  * pas de test de validation du code

Il est alors facile de trouver ce qu'il faut modifier pour ne coder que les mots d'au moins 5 caractères.
```python
LONGUEUR_MOT_MIN = 5
```

# Sixième version : documenter

[Il faut **documenter/spécifier** chaque fonction.](code_cesar/fonction_lisible_decomposition_constantes_documentation.py) 

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * une fonction paramétrée, plus facilement réutilisable
>  * séparation du calcul et de l'affichage : le calcul peut être réutilisé
>  * de petites fonctions dont le code est facile à lire et donc à faire évoluer
>  * la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
>  * des constantes nommées, donc plus explicites et plus faciles à changer
>  * **une documentation permet la compréhension du rôle de chaque fonction et son utilisation**
>  
> **-**
>  * pas de test de validation du code
 
Utilisation des <q>docstring</q>. Il faut décrire :
 * ce que fait la fonction,
 * le rôle de chaque paramètre et préciser son type,
 * la nature du résultat et son type (quand il y en a),
 * éventuellement préciser des conditions d'utilisation,
 * éventuellement indiquer les effets de bord.


On obtient facilement des informations sur une fonction :
```
>>> help(code_mot)
```


# Septième version : tester

[Il faut **tester** pour valider chaque fonction.](code_cesar/fonction_lisible_decomposition_constantes_documentation_tests.py) 

> **+**
>  * des identificateurs lisibles et significatifs : un code plus compréhensible
>  * une fonction paramétrée, plus facilement réutilisable
>  * séparation du calcul et de l'affichage : le calcul peut être réutilisé
>  * de petites fonctions dont le code est facile à lire et donc à faire évoluer
>  * la décomposition du code est plus explicite, les commentaires dans le code deviennent superflus
>  * des constantes nommées, donc plus explicites et plus faciles à changer
>  * une documentation permet la compréhension du rôle de chaque fonction et son utilisation
>  * **des tests qui permettent une validation du code de chaque fonction, et en plus fournissent des exemples d'utilisation** 
>  
> **-**
>  * **RAS**

Attention à la sensibilité des tests (problème des espaces par exemple). Utiliser les différentes options de `doctest`
```python
doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
```

Dans l'idéal, pour programmer une fonction il faut :

 1 écrire sa documentation
 2 écrire les tests qui permettront de valider la fonction
 3 écrire le code
 4 vérifier que le code passe les tests avec succès, sinon recommencer en 3

# Ultime(?) version

Il faut séparer ce que l'on exécuter des fonctions définies que l'on peut mettre à disposition d'autres programmes.

[Vers la programmation modulaire](code_cesar/code_cesar.py)

On peut utiliser le fichier

 * comme un programme. Voir alors l'utilisation des arguments sur la ligne de commande avec Thonny : utilisation de `import.sys` et gestion des arguments.
 * comme un  module dont les [fonctions sont utilisées par un autre programme](code_cesar/code13_fichier.py).
   ```python
   import code_cesar as cesar

   def code_fichier(fichier_texte):
      ...
            destination.write(cesar.code_phrase(ligne_texte,13)+'\n')
   ```

# Deboguer

* Mode pas à pas de Thonny.
   * petits pas / grands pas
   * intéressant pour appels de fonctions : environnements différents, variables locales

* points d'arrêts



